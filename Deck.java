import java.util.Random;

public class Deck {
	int numberOfCards;
	Card[] stack;
	Random rng;
	
	public Deck() {
		rng = new Random(53);
		this.stack = new Card[52];
		String[] suit = new String[]{"Hearts", "Spades", "Clubs", "Diamonds"};
		int[] rank = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
		
		this.numberOfCards = this.stack.length;	
		
		int i = 0;
		for (int r : rank){
			for (String s : suit){
				stack[i] = new Card(s,r);
				i++;
			}
		}
	}
	
	public int length(){
		return this.numberOfCards;
	}
	
	public Card drawTopCard(){
		numberOfCards--;
		return stack[this.numberOfCards];
	}
	
	public String toString(){
		String printer = "This is all the cards in the deck : \n";
	
		for (Card c : this.stack){
			printer += c.toString() + "\n";
		}
		return printer;
	}
	
	public void shuffle(){
		for (int i = 0; i < numberOfCards; i++){
			int random = rng.nextInt(this.numberOfCards);
			Card temp = this.stack[random];
			this.stack[random] = this.stack[i];
			this.stack[i] = temp;
		}
	}
}