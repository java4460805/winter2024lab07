public class SimpleWar {
	public static void main(String[] args) {
		Deck Pile = new Deck();
		Pile.shuffle();
		
		double scoreP1 = 0.0;
		double scoreP2 = 0.0;
		
		while (Pile.length() > 0) {
			Card cardDrawn = Pile.drawTopCard();
			System.out.println(cardDrawn);
			System.out.println(cardDrawn.calculateScore());
			
			Card cardDrawn2 = Pile.drawTopCard();
			System.out.println(cardDrawn2);
			System.out.println(cardDrawn2.calculateScore());
			
			if (cardDrawn2.calculateScore() > cardDrawn.calculateScore()){
				System.out.println(cardDrawn2.calculateScore());
				scoreP2 += 1;
			}
		
			if (cardDrawn2.calculateScore() < cardDrawn.calculateScore()){
				System.out.println(cardDrawn.calculateScore());
				scoreP1 += 1;
			}
		}
		
		if (scoreP1 > scoreP2)
			System.out.println("Congrats player 1 with " + scoreP1);
		
		if (scoreP1 < scoreP2)
			System.out.println("Congrats player 2 with " + scoreP2);
	}
	
}