public class Card {
	String suit;
	int rank;
	
	public Card(String suit, int rank) {
		this.suit = suit;
		this.rank = rank;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public int getRank(){
		return this.rank;
	}
	
	public String toString(){
		String printRank = Integer.toString(rank);
		if (rank == 1)
			printRank = "Ace";
		if (rank == 11)
			printRank = "Jack";
		if (rank == 12)
			printRank = "Queen";
		if (rank == 13)
			printRank = "King";

		return printRank + " of " + suit;
	}
	
	public double calculateScore() {
		double score = 0;
		
		if (suit.equals("Hearts"))
			score = rank + 0.4;
		if (suit.equals("Spades"))
			score = rank + 0.3;
		if (suit.equals("Diamonds"))
			score = rank + 0.2;
		if (suit.equals("Clubs"))
			score = rank + 0.1;
		
		return score;
		
	}
}